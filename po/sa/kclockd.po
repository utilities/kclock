# Sanskrit translations for kclock package.
# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the kclock package.
# Kali <EMAIL@ADDRESS>, 2024.
#
# SPDX-FileCopyrightText: 2024 kali <shreekantkalwar@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: kclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-07 00:43+0000\n"
"PO-Revision-Date: 2024-12-24 20:39+0530\n"
"Last-Translator: kali <shreekantkalwar@gmail.com>\n"
"Language-Team: Sanskrit <kde-i18n-doc@kde.org>\n"
"Language: sa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>2);\n"
"X-Generator: Lokalize 24.08.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "श्रीकान्त् कलवार्"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "skkalwar999@gmail.com"

#: alarm.cpp:73 alarm.h:51
#, kde-format
msgid "Alarm"
msgstr "आपत्सङ्केत"

#: alarm.cpp:77 timer.cpp:67
#, kde-format
msgid "View"
msgstr "दृश्यं"

#: alarm.cpp:80
#, kde-format
msgid "Dismiss"
msgstr "उत्सृज्"

#: alarm.cpp:83
#, kde-format
msgid "Snooze"
msgstr "स्नूज"

#: alarmmodel.cpp:222
#, kde-kuit-format
msgctxt "@info"
msgid "Alarm: <shortcut>%1 %2</shortcut>"
msgstr "अलार्म: <shortcut>%1 %2</shortcut>"

#: main.cpp:31
#, kde-format
msgid "Don't use PowerDevil for alarms if it is available"
msgstr "यदि उपलब्धं भवति तर्हि अलार्मस्य कृते PowerDevil इत्यस्य उपयोगं न कुर्वन्तु"

#: main.cpp:46
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 केडीई समुदाय"

#: main.cpp:47
#, kde-format
msgid "Devin Lin"
msgstr "डेविन् लिन्"

#: main.cpp:48
#, kde-format
msgid "Han Young"
msgstr "हान यंग"

#: timer.cpp:62
#, kde-format
msgid "Timer complete"
msgstr "समयनिर्धारक सम्पूर्ण"

#: timer.cpp:63
#, kde-format
msgid "Your timer %1 has finished!"
msgstr "भवतः समयनिर्धारकः %1 समाप्तः!"

#: xdgportal.cpp:34
#, kde-format
msgid ""
"Allow the clock process to be in the background and launched on startup."
msgstr "घण्टाप्रक्रिया पृष्ठभूमितः भवितुं स्टार्टअप-समये च प्रारम्भं कर्तुं अनुमन्यताम् ।"
